/**
 * Every piece of UI that requires javascript should have its own
 * javascript file. Use this file as a template for structuring
 * all JS source files.
 *
 * {Document Title}
 * {Description}
 */

( function( window, jQuery ) {
	'use strict';
	var document = window.document;

    var example = {

        init: function() {
            // Do something...
          
        }

    };

    jQuery(document).ready(function(){
        example.init();
    });

} )( window, jQuery );
