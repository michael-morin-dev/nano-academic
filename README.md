# WP Base Theme

## Description

Wordpress Base Theme created by **Agence CC**.

## Gulp installation and usage
Gulp comes prepackaged in this theme. All you need to do is install dependencies `npm install`, then compile prepackage styles and scripts (`gulp`) and you can then run `gulp watch` to run the watch task from the gulp taskfile. (You can find the list of tasks in the gulpfile.js)

 1. npm install
 2. gulp
 3. gulp watch

---
*This theme was tested with npm 6.14.2, node 8.16.0, Ruby Sass 3.7.4 and Gulp 3.9.1 (CLI 2.2.0). If you have different versions, some dependencies might not be working. You can use [NVM](https://github.com/nvm-sh/nvm)  to manage different versions.*

## Theme installation and usage
This theme comes with a predefined project scaffolding.
 `/assets` contains both the src and the dist for styles, scripts, images and fonts. *(`/dist` is excluded from git.)* 
`/includes` contains theme settings; Core settings, menus and autoloading.
`/libraries` contains all external libraries
`/page-templates` contains all page templates
`/partials` contains all partial views

>  `/assets/src/images` should only contains static images (logos, icons, etc.). Any file that can be changed by a user should be added to the **Media Library**.

## Wordpress Coding Standards
[Wordpress Coding Standards](https://make.wordpress.org/core/handbook/best-practices/coding-standards/)

While there is no strict enforcing of the WCS, we recommend the following rules when developping on Wordpress.

#### Page Templates must have the following heading:

    <?php
    	/**
    	 * Template Name: {Template name}
    	 */
     ?>
---
#### Files or partials include must use the Wordpress native function
    get_template_part()
---
#### Make use of secondary header and footer
 When creating a multi-versioned header and/or footer, use the Wordpress native function to include them in your page

    File: header-secondary.php
    <?php get_header('secondary'); ?>
    ---
    File: footer-secondary.php
    <?php get_footer('secondary'); ?>

---
*This list is not exclusive and may be updated at any time.*