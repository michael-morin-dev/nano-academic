<?php get_header(); ?>

<?php
    if (have_rows('page_header')) :
        while ( have_rows('page_header') ) : the_row();
            $moduleType = get_row_layout();
            $moduleTypeFilePath = sprintf( get_stylesheet_directory() . '/partials/headers/%s.php', $moduleType );
            if( file_exists( $moduleTypeFilePath ) ) :
                include $moduleTypeFilePath;
            endif;
        endwhile;
    endif;
?>

<div class="site-content">
    <?php
        if (have_rows('page_blocks')) :
            while ( have_rows('page_blocks') ) : the_row();
                $moduleType = get_row_layout();
                $moduleTypeFilePath = sprintf( get_stylesheet_directory() . '/partials/blocks/%s.php', $moduleType );
                if( file_exists( $moduleTypeFilePath ) ) :
                    include $moduleTypeFilePath;
                endif;
            endwhile;
        endif;
    ?>
</div>

<?php get_footer(); ?>