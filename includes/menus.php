<?php
namespace THEME_WP\Menus;

/**
 * Set up menus
 *
 * @return void
 */
function setup() {
	$n = function ( $function ) {
		return __NAMESPACE__ . "\\$function";
	};

	register_nav_menus( array(
		'header_menu' => 'Header Menu',
		'footer_menu' => 'Footer Menu',
		'mobile_menu' => 'Mobile Menu',
	) );
}