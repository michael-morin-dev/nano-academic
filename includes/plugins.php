<?php
namespace THEME_WP\Plugins;

/**
 * Overrides settings coming from any plugins
 *
 * @return void
 */
function setup() {
	$n = function ( $function ) {
		return __NAMESPACE__ . "\\$function";
	};

	add_action( 'admin_enqueue_scripts', $n( 'enqueue_admin_scripts' ) );
	
	// ACF
	add_filter( 'acf/fields/flexible_content/layout_title/name=page_header', $n('acf_flexible_content_preview'), 10, 4 );
	add_filter( 'acf/fields/flexible_content/layout_title/name=page_blocks', $n('acf_flexible_content_preview'), 10, 4 );

    add_filter( 'acf_icon_path_suffix', $n('acf_icon_path_suffix') );

	// Move Yoast to bottom
	add_filter( 'wpseo_metabox_prio', $n('yoasttobottom') );

	// Remove wpautop from contact form 7
	add_filter( 'wpcf7_autop_or_not', '__return_false' );
}

/**
 * Enqueue styles for admin
 */
function enqueue_admin_scripts($hook) {
    // ACF
    wp_enqueue_style( 'acf-admin', THEME_URL . '/libraries/acf/acf-admin.css', array(), THEME_VERSION );

    wp_register_script( 'acf-admin', THEME_URL . '/libraries/acf/acf-admin.js', array(), null, true );
    wp_localize_script( 'acf-admin', 'theme_location', get_stylesheet_directory_uri() );
    wp_enqueue_script( 'acf-admin' );
}

function acf_flexible_content_preview( $title, $field, $layout, $i ) {
    $thumb_path = get_stylesheet_directory() . '/libraries/acf/previews/' . $layout['name'] . '.jpg';
    $thumb_url = get_stylesheet_directory_uri() . '/libraries/acf/previews/' . $layout['name'] . '.jpg';
    
    if(file_exists($thumb_path)):
        $title = '<img src="' . $thumb_url . '" /> ' . $title;
    endif;
    
    return $title;
}

function bdeb_register_mce_buttons( $buttons ) {	
	$buttons[] = 'mark';

	return $buttons;
}

function yoasttobottom() {
	return 'low';
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

function acf_icon_path_suffix( $path_suffix ) {
    return 'assets/dist/images/icons/';
}