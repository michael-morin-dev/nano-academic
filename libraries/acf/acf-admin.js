jQuery(document).ready(function () {
    acf_display_flexible_layouts_preview_image();
    acf_collapse_flexible_layouts();

    function acf_collapse_flexible_layouts(){
        jQuery('.acf-flexible-content .layout').addClass('-collapsed');
    }

    function acf_display_flexible_layouts_preview_image(){
        jQuery("a[data-name=add-layout]").click(function(){
            waitForEl(".acf-tooltip li", function() {
                jQuery(".acf-tooltip li a").hover(function(){
                    imageTP = jQuery(this).attr("data-layout");

                    jQuery(".acf-tooltip").append("<div class='acf-fc-preview-image'><img src='" + theme_location + "/libraries/acf/previews/" + imageTP + ".jpg'></div>");

                    jQuery(".acf-fc-preview-image").css({
                        top: jQuery(this).parent().position().top
                    });
                }, function(){
                        jQuery(".acf-fc-preview-image").remove();
                    });
                });
            })
            
            var waitForEl = function(selector, callback) {
                if (jQuery(selector).length) {
                    callback();
                } else {
                    setTimeout(function() {
                    waitForEl(selector, callback);
                }, 100);
            }
        };
    }
});