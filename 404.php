<?php get_header(); ?>

<div class="site-content">
	<section class="section section--404">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h3>Erreur 404 - page introuvable</h3>

					<h5>Désolé, il est impossible de trouver la page.</h5>
					<p>L'adresse que vous tentez d'atteindre est introuvable ou inaccessible en ce moment.</p>

					<a href="#" onclick="window.history.back(); return false;" class="btn btn-primary">Retourner à la page précédente</a>
					<a href="<?php echo get_site_url(); ?>" class="btn btn-primary">Aller à l'accueil</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>