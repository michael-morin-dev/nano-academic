<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<?php wp_head(); ?>

		<link rel="dns-prefetch" href="//cdn.domain.com/">
		<link rel="dns-prefetch" href="//fonts.googleapis.com/">
		<link rel="dns-prefetch" href="//www.google-analytics.com">
	</head>
	<body <?php body_class(); ?>>
		<?php wp_body_open(); ?>