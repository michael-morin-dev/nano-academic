const gulp = require('gulp');
const gulpIf = require('gulp-if');
const sass = require('gulp-sass');
const cssmin = require('gulp-cssmin');
const jsmin = require('gulp-jsmin');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const jsImport = require('gulp-js-import');
const sourcemaps = require('gulp-sourcemaps');
const clean = require('gulp-clean');
const rename = require('gulp-rename');
const isProd = process.env.NODE_ENV === 'prod';

function styles() {
    return gulp.src('assets/src/css/main.scss')
        .pipe(gulpIf(!isProd, sourcemaps.init()))
        .pipe(sass({
            includePaths: ['node_modules']
        }).on('error', sass.logError))
        .pipe(gulpIf(!isProd, sourcemaps.write()))
        .pipe(gulp.dest('assets/dist/css/'))
        .pipe(cssmin())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulpIf(!isProd, sourcemaps.write()))
        .pipe(gulp.dest('assets/dist/css/'));
}

function scripts() {
    return gulp.src('assets/src/js/*.js')
        .pipe(jsImport({
            hideConsole: true
        }))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('assets/dist/js/'))
        .pipe(gulpIf(isProd, jsmin()))
        .pipe(jsmin())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest('assets/dist/js/'));
}

function images() {
    return gulp.src('assets/src/images/*')
        .pipe(gulpIf(isProd, imagemin()))
        .pipe(gulp.dest('assets/dist/images/'));
}

function fonts() {
    return gulp.src(['assets/src/fonts/*'])
        .pipe(gulp.dest('assets/dist/fonts/'));
}

function watchFiles() {
    gulp.watch('assets/src/**/*.scss', gulp.series(styles));
    gulp.watch('assets/src/**/*.js', gulp.series(scripts));
    gulp.watch('assets/src/images/**/*.*', gulp.series(images));
    gulp.watch('assets/src/fonts/**/*.*', gulp.series(fonts));
    return;
}

exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.fonts = fonts;
exports.watch = gulp.parallel(styles, scripts, images, fonts, watchFiles);
exports.default = gulp.series(styles, scripts, images, fonts);